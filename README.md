# Golang Parser

Sample to improve parsing JSON with nested JSON objects.

Task:  
Using API https://covidtracker.bsg.ox.ac.uk/about-api get all data about “By country over time" for current year and store it into your DB: date_value, country_code, confirmed, deaths, stringency_actual, stringency. Output the data by country_code (the country_code is set) in form of a table and sort them by deaths (or date_value which is the same) in ascending order.

Getting data with curl on bash:
```bash
curl https://covidtrackerapi.bsg.ox.ac.uk/api/v2/stringency/date-range/2022-02-06/2022-02-09 | jq '.'
curl https://covidtrackerapi.bsg.ox.ac.uk/api/v2/stringency/date-range/2022-02-06/2022-02-09 | jq '.data' | head -50
```
