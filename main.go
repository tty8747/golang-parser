package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"time"

	"github.com/savaki/jq"
)

func main() {
	//const link = "https://covidtrackerapi.bsg.ox.ac.uk/api/v2/stringency/date-range/2022-02-06/2022-02-09"
	const link = "https://covidtrackerapi.bsg.ox.ac.uk/api/v2/stringency/date-range/2022-01-01/2022-05-31"

	// gets rawData
	rawData := getData(link)

	// parses rawData, find countries
	countries := new(string)
	parseDataSimple(rawData, ".countries", countries)

	// print list of countries
	fmt.Println(*countries)
	fmt.Println(reflect.TypeOf(*countries))
	time.Sleep(3 * time.Second)

	// gets list of countries
	type countriesList struct {
		List []string `json:"countries"`
	}
	var cl countriesList
	if err := json.Unmarshal([]byte(*countries), &cl.List); err != nil {
		panic(err)
	}
	for _, elem := range cl.List {
		fmt.Println("->", elem)
	}

	// It's used to exit with 123 error code
	//	os.Exit(123)

	time.Sleep(1 * time.Second)

	// parses rawData
	date := fmt.Sprintf(".data.%s", "2022-02-09")
	result := new(string)
	go parseDataSimple(rawData, date, result)

	// print unmarshal data
	fmt.Println(*result)
	time.Sleep(3 * time.Second)

	// gets all data
	resultAllData := new([]string)
	for _, elem := range genListOfDates() {
		date := fmt.Sprintf(".data.%s", elem)
		go parseData(rawData, date, resultAllData)
	}

	// help message
	fmt.Println("All data are got!")
	time.Sleep(3600 * time.Millisecond)

	// print all result data
	for idx, elem := range *resultAllData {
		fmt.Println(idx, "=>", elem)
	}

	// gets all data for struct
	resultAllDataIntoStruct := new(Main)
	for _, elem := range genListOfDates() {
		date := fmt.Sprintf(".data.%s", elem)
		// go parseDataIntoStruct(rawData, date, cl.List, resultAllDataIntoStruct)
		parseDataIntoStruct(rawData, date, cl.List, resultAllDataIntoStruct)
	}

	// help message
	fmt.Println("All data are got, again!")
	for i := 5; i <= 0; i-- {
		fmt.Println("Waiting for", i)
		time.Sleep(1 * time.Second)
	}

	// print all result data
	fmt.Println("~>", *resultAllDataIntoStruct)
	for _, elem := range resultAllDataIntoStruct.Data {
		fmt.Printf("date_value: %s, country_code: %s, confirmed: %d, deaths: %d, stringency_actual: %f, stringency: %f, stringency_legacy: %f, stringency_legacy_disp: %f\n", elem.DateValue, elem.CountryCode, elem.Confirmed, elem.Deaths, elem.StringencyActual, elem.Stringency, elem.StringencyLegacy, elem.StringencyLegacyDisp)
	}
}

// Gets raw data
func getData(s string) []byte {
	response, err := http.Get(s)
	if err != nil {
		panic(err)
	}

	defer response.Body.Close()
	rawData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	return rawData
}

// Parses data simple
func parseDataSimple(rawData []byte, tail string, result *string) {
	op, _ := jq.Parse(tail)
	value, _ := op.Apply(rawData)
	valueType := reflect.TypeOf(value)
	fmt.Println(valueType)
	*result = string(value)
}

// Parses data
func parseData(rawData []byte, tail string, result *[]string) {
	op, _ := jq.Parse(tail)
	value, _ := op.Apply(rawData)
	valueType := reflect.TypeOf(value)
	fmt.Println(valueType)
	*result = append(*result, string(value))
}

// Parses data into struct
func parseDataIntoStruct(rawData []byte, tail string, countries []string, result *Main) {
	op, _ := jq.Parse(tail)
	value, _ := op.Apply(rawData)

	for _, elem := range countries {
		op2, _ := jq.Parse(elem)
		val2, _ := op2.Apply(value)
		fmt.Println(">>>", string(val2))
		if string(val2) == "" {
			continue
		}

		var soloResult Data
		// The argument to Unmarshal must be a non-nil pointer
		if err := json.Unmarshal([]byte(val2), &soloResult); err != nil {
			panic(err)
		}
		result.Data = append(result.Data, soloResult)
	}
}

// Gets list of dates
func rangeDate(start, end time.Time) func() time.Time {
	y, m, d := start.Date()
	start = time.Date(y, m, d, 0, 0, 0, 0, time.UTC)
	y, m, d = end.Date()
	end = time.Date(y, m, d, 0, 0, 0, 0, time.UTC)

	return func() time.Time {
		if start.After(end) {
			return time.Time{}
		}
		date := start
		start = start.AddDate(0, 0, 1)
		return date
	}
}

// Gets list of dates
func genListOfDates() (listOfDates []string) {
	end := time.Now()
	start, err := time.ParseInLocation("2006-01-02", fmt.Sprintf("%s-%s-%s", end.Format("2006"), "01", "01"), time.Local)
	if err != nil {
		panic(err)
	}

	for rd := rangeDate(start, end); ; {
		date := rd()
		if date.IsZero() {
			break
		}
		listOfDates = append(listOfDates, date.Format("2006-01-02"))
	}
	return listOfDates
}

// Nested struct example
/*
type Data struct {
	Date struct {
		Country struct {
			DateValue            string  `json:"date_value"`
			CountryCode          string  `json:"country_code"`
			Confirmed            int     `json:"confirmed"`
			Deaths               int     `json:"deaths"`
			StringencyActual     float64 `json:"stringency_actual"`
			Stringency           float64 `json:"stringency"`
			StringencyLegacy     float64 `json:"stringency_legacy"`
			StringencyLegacyDisp float64 `json:"stringency_legacy_disp"`
		} `json:"country"`
	} `json:"date"`
}
*/

type Main struct {
	Data []Data
}

type Data struct {
	DateValue            string  `json:"date_value"`
	CountryCode          string  `json:"country_code"`
	Confirmed            int     `json:"confirmed"`
	Deaths               int     `json:"deaths"`
	StringencyActual     float64 `json:"stringency_actual"`
	Stringency           float64 `json:"stringency"`
	StringencyLegacy     float64 `json:"stringency_legacy"`
	StringencyLegacyDisp float64 `json:"stringency_legacy_disp"`
}
